import * as fromLogin from './modules/login/reducers/login.reducer';
import * as fromCamera from './modules/camera/reducers/camera.reducer';

export interface State {
    loginResult: fromLogin.State;
    cameraResult : fromCamera.State;
}

export const reducers = {
    loginResult: fromLogin.reducer,
    cameraResult : fromCamera.reducer
};

export function selectLoginSuccess(state: State) {
    return state.loginResult.result;
}

export function selectLoginFailure(state: State) {
    return state.loginResult.err;
}

export function selectCameraSuccess(state: State) {
    return state.cameraResult.result;
}

export function selectCameraFailure(state: State) {
    return state.cameraResult.err;
}
