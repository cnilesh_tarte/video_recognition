import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Router } from '@angular/router';
import { catchError } from 'rxjs/operators';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    constructor(private router: Router) { }
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add authorization header with jwt token if available
        if (localStorage.getItem('auth') != "undefined") {
            const currentUser = localStorage.getItem('username');
            if (currentUser ) {
                request = request.clone({
                    setHeaders: {
                        Authorization: `Bearer ${currentUser}`
                    }
                });
            } else {
                this.router.navigate(['/']);
            }
        }

        return next.handle(request).pipe(
            catchError(
                (err, caught) => {
                    if (err == 'jwt expired') {
                        this.handleAuthError();
                        return of(err);
                    }
                    throw err;
                }
            )
        );
    }
    private handleAuthError() {
        localStorage.removeItem('auth');
        this.router.navigate(['/']);
    }
}
