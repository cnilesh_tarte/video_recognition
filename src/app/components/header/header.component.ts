import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  status: boolean = false;
  
  constructor(
    private router: Router,
    public translate: TranslateService,
  ) {
    let selectedLanguage =  localStorage.getItem('language');
    translate.addLangs([ selectedLanguage ,'en', 'de']);
    translate.setDefaultLang(selectedLanguage);
   }

  
  switchLang(lang: string) {
    localStorage.setItem('language', lang);
    this.translate.use(lang);
  }

  ngOnInit() {
  }


  clickEvent(){
      this.status = !this.status;       
  }

  logout(){
    this.router.navigateByUrl('login');
    localStorage.clear();
  }
}
