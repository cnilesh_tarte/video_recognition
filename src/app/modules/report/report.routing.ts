import { NgModule, ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HeaderComponent } from 'src/app/components/header/header.component';
import { ReportComponent } from './report.component';

export const routes: Routes = [
    { path: '',
        component: HeaderComponent,
        pathMatch: "full",
        children: [
            { path: '', component: ReportComponent,  pathMatch: "full" },
        ]
   }
]


export const reportRouting: ModuleWithProviders = RouterModule.forChild(routes);