import { NgModule, ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HeaderComponent } from 'src/app/components/header/header.component';
import { SettingsComponent } from './settings.component';

export const routes: Routes = [
    { path: '',
        component: HeaderComponent,
        pathMatch: "full",
        children: [
            { path: '', component: SettingsComponent,  pathMatch: "full" },
        ]
   }
]


export const settingsRouting: ModuleWithProviders = RouterModule.forChild(routes);