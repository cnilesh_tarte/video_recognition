import { Component, OnInit, OnDestroy } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup, Validators  } from '@angular/forms';
import { LoginService } from './login.service';
import { Router } from '@angular/router';

 
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import * as fromRoot from './../../reducers';
import { Store } from '@ngrx/store';

import * as LoginActions from './actions/login.actions';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy{
  loginForm: FormGroup;
  status: boolean = false;

  obsLogin: Observable<any>;
  subLogin: Subscription;
  obsLoginFailure: Observable<any>;
  subLoginFailure: Subscription;

  username: string;
  password:string;

  constructor(
    private _store: Store<fromRoot.State>,
    public translate: TranslateService,
    private fb: FormBuilder,
    private loginService: LoginService,
    private router: Router
  ) {

    this.loginForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });

    translate.addLangs(['en', 'de']);
    translate.setDefaultLang('en');

    this.obsLogin = this._store.select(fromRoot.selectLoginSuccess);
    this.obsLoginFailure = this._store.select(fromRoot.selectLoginFailure);
  
  }

  switchLang(lang: string) {
    localStorage.setItem('language', lang);
    this.translate.use(lang);
  }

  ngOnInit() {
    localStorage.setItem('language', 'en');

    this.subLogin = this.obsLogin.subscribe(res => {
        if (res.status === 200) {
            let response = res.body.data;
            if(response.username === this.username && response.password === this.password){
              this.status = false;       
              localStorage.setItem('username', response.username);
              
              this.router.navigateByUrl('camera');
            }else{
              this.status = true;       
            }
        }
    })

    this.subLoginFailure = this.obsLoginFailure.subscribe(err => {
      if (err) {
        console.log('something went wrong');
      }
    })

  }

  validateUser(){
    if(this.loginForm.valid){
      this.username = this.loginForm.controls['username'].value;
      this.password = this.loginForm.controls['password'].value;
      this._store.dispatch(new LoginActions.Login(this.loginForm.value));
    }
  }

  ngOnDestroy() {
    if (this.subLogin) this.subLogin.unsubscribe();
    if (this.subLoginFailure) this.subLoginFailure.unsubscribe();
  }

}
