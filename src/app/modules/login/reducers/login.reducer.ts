import { Response } from '@angular/http';
import * as LoginActions from './../actions/login.actions';

export interface State {       
    result: any,
    err: any
}

const initialState: State = {    
    result: {},
    err: {}
};

export function reducer(state = initialState, action: LoginActions.All): State {
    switch (action.type) {
        case LoginActions.LOGINACTION_SUCCESS: {
            return {
                ...state,
                result: action.token
            }
        }
        case LoginActions.LOGINACTION_FAILURE: {
            return {
                ...state,
                err: action.err
            }
        }          

        default: {
            return state;
        }
    }

}
