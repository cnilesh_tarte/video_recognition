import { Action } from '@ngrx/store';
import { Response } from '@angular/http';

export const LOGINACTION = '[LOGIN] TOKEN';
export const LOGINACTION_SUCCESS = '[LOGIN] TOKEN SUCCESS';
export const LOGINACTION_FAILURE = '[LOGIN] TOKEN FAILURE';


export class Login {
    readonly type = LOGINACTION;
    constructor(public loginInfo: any) { }
}

export class LoginSuccess implements Action {
    readonly type = LOGINACTION_SUCCESS;
    constructor(public token: any) { }
}

export class LoginFailure implements Action {
    readonly type = LOGINACTION_FAILURE;
    constructor(public err: any) {
       
    }
}

export type All = Login | LoginSuccess | LoginFailure 