
import { LoginService } from './login.service';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import { Action } from '@ngrx/store';
import * as LoginActions from './actions/login.actions';

@Injectable()
export class LoginEffects {

    constructor(
        private actions$: Actions,
        private loginService: LoginService
    ) { }

    @Effect()
    loginAction$: Observable<Action> = this.actions$.ofType<LoginActions.Login>(LoginActions.LOGINACTION)
        .map(action => action)
        .switchMap(
        payload => this.loginService.loginUser(payload.loginInfo)
            .map(results => new LoginActions.LoginSuccess(results))
            .catch(err => Observable.of(new LoginActions.LoginFailure(err)))
        );
}
