import { Response } from '@angular/http';
import * as CameraActions from './../actions/camera.actions';

export interface State {       
    result: any,
    err: any
}

const initialState: State = {    
    result: {},
    err: {}
};

export function reducer(state = initialState, action: CameraActions.All): State {
    switch (action.type) {
        case CameraActions.CAMERAACTION_SUCCESS: {
            return {
                ...state,
                result: action.token
            }
        }
        case CameraActions.CAMERAACTION_FAILURE: {
            return {
                ...state,
                err: action.err
            }
        }          

        default: {
            return state;
        }
    }

}
