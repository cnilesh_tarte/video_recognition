import { Component, OnInit, OnDestroy} from '@angular/core';
import { CameraService } from './camera.service';
import { TranslateService } from '@ngx-translate/core';

import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import * as fromRoot from './../../reducers';
import { Store } from '@ngrx/store';

import * as CameraActions from './actions/camera.actions';

@Component({
  selector: 'app-camera',
  templateUrl: './camera.component.html',
  styleUrls: ['./camera.component.css']
})
export class CameraComponent implements OnInit, OnDestroy {

  cameraOne = [];
  cameraTwo = [];

  obsCamera: Observable<any>;
  subCamera: Subscription;
  obsCameraFailure: Observable<any>;
  subCameraFailure: Subscription;

  constructor(
    private _store: Store<fromRoot.State>,
    private cameraService: CameraService,
  ) { 
    this.obsCamera = this._store.select(fromRoot.selectCameraSuccess);
    this.obsCameraFailure = this._store.select(fromRoot.selectCameraFailure);
  }

  ngOnInit() {
    let i = 0;
    setInterval (() => {
      this.getCameraOutput(i);
      if( i == 1) {
        i = 0;
      }else{
        i= 1;
      }
    }, 500);


    this.subCamera = this.obsCamera.subscribe(res => {
      if (res.status === 200) {
        this.cameraOne = res.body.cameraOne;
        this.cameraTwo= res.body.cameraTwo; 
      }
    })

    this.subCameraFailure = this.obsCameraFailure.subscribe(err => {
      if (err) {
        console.log('something went wrong');
      }
    })

  }
  
  getCameraOutput(i){
    this._store.dispatch(new CameraActions.Camera(i));
  }

  getClass(result){
    if(result == 'pass'){
      return 'fa fa-thumbs-up';
    }else{
      return 'fa fa-thumbs-down';
    }
  }

  ngOnDestroy() {
    if (this.subCamera) this.subCamera.unsubscribe();
    if (this.subCameraFailure) this.subCameraFailure.unsubscribe();
  }
  
}
