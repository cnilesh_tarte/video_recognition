import { Action } from '@ngrx/store';
import { Response } from '@angular/http';

export const CAMERAACTION = '[LOGIN] ';
export const CAMERAACTION_SUCCESS = '[CAMERA]  SUCCESS';
export const CAMERAACTION_FAILURE = '[CAMERA]  FAILURE';


export class Camera {
    readonly type = CAMERAACTION;
    constructor(public i: number) { }
}

export class CameraSuccess implements Action {
    readonly type = CAMERAACTION_SUCCESS;
    constructor(public token: any) { }
}

export class CameraFailure implements Action {
    readonly type = CAMERAACTION_FAILURE;
    constructor(public err: any) {
       
    }
}

export type All = Camera | CameraSuccess | CameraFailure 