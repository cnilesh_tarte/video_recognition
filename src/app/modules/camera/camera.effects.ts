
import { CameraService } from './camera.service';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import { Action } from '@ngrx/store';
import * as CameraActions from './actions/camera.actions';

@Injectable()
export class CameraEffects {

    constructor(
        private actions$: Actions,
        private cameraService: CameraService
    ) { }

    @Effect()
    loginAction$: Observable<Action> = this.actions$.ofType<CameraActions.Camera>(CameraActions.CAMERAACTION)
        .map(action => action)
        .switchMap(
        payload => this.cameraService.getCameraOutput(payload.i)
            .map(results => new CameraActions.CameraSuccess(results))
            .catch(err => Observable.of(new CameraActions.CameraFailure(err)))
        );
}
