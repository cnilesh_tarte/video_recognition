import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CameraComponent } from './camera.component';
import { Routes, RouterModule } from '@angular/router';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { HeaderModule } from 'src/app/components/header/header.module';
import { AuthService } from 'src/app/auth/auth.service';
import { cameraRouting } from './camera.routing';
import { CameraService } from './camera.service';
import { EffectsModule } from '@ngrx/effects';
import { CameraEffects } from './camera.effects';
import { InterceptorModule } from 'src/app/helpers/interceptor.module';

@NgModule({
  declarations: [CameraComponent],
  imports: [
    CommonModule,
    cameraRouting,
    HttpClientModule,
    EffectsModule.forFeature([CameraEffects]),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: httpTranslateLoader,
        deps: [HttpClient]
      }
    }),
    InterceptorModule,
    HeaderModule
  ],
  exports: [CameraComponent],
  providers: [CameraService]
})
export class CameraModule { }


// AOT compilation support
export function httpTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http);
}