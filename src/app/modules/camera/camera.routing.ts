import { NgModule, ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HeaderComponent } from 'src/app/components/header/header.component';
import { CameraComponent } from './camera.component';

export const routes: Routes = [
    { path: '',
        component: HeaderComponent,
        pathMatch: "full",
        children: [
            { path: '', component: CameraComponent,  pathMatch: "full" },
        ]
   }
]


export const cameraRouting: ModuleWithProviders = RouterModule.forChild(routes);